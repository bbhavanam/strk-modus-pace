/**
 * 
 */
package com.sitetracker.utils;

import java.util.HashMap;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

/**
 * @author sitetracker This class is used to parse ObjectIds String to a
 *         corresponding HashMap.
 */
public class ParseObjectIds extends AbstractMessageTransformer {

	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		HashMap<Object, Object> configMap = (HashMap<Object, Object>) message.getPayload();
		HashMap<String, String> map = new HashMap<>();
			String objectIds = (String) configMap.get("Object_External_Ids__c");
			if (objectIds != null){
				String arr[] = objectIds.split(",");
				for (String s : arr) {
					String[] arr2 = s.split(":");
					map.put(arr2[0], arr2[1]);
				}
				message.setInvocationProperty("objectIDMap", map);
			}
			System.out.println("objectIds " + objectIds);
		return map;
	}

}
