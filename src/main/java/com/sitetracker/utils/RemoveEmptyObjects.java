package com.sitetracker.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class RemoveEmptyObjects extends AbstractMessageTransformer {
	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		List<HashMap<Object, Object>> dataIn = (List<HashMap<Object, Object>>) message.getPayload();
		String sObject = (String) message.getInvocationProperty("sObject");
		List<HashMap<Object, Object>> dataOut = new ArrayList<HashMap<Object, Object>>();
		
		
		    for (HashMap<Object, Object> eachObj :dataIn) {
		    	if(!eachObj.isEmpty()) {
		    		if("strk__Project__c".equals(sObject)){
		    			
		    			eachObj.remove("FA_Code__c");
		    	}/* else if("AT_T_LTE_Project__c".equals(sObject) || "AT_T_NSB_Project__c".equals(sObject)){
		    			eachObj.remove("PACE_ID_Number__c");
		    	}*/
		    		
		    		dataOut.add(eachObj);
		    	}
		    }
		       
		   
		return dataOut;
	}

}
