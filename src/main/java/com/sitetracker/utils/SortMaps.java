package com.sitetracker.utils;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class SortMaps extends AbstractMessageTransformer {

	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		HashMap<String, List<HashMap<String, Object>>> unSortedMap = (HashMap<String, List<HashMap<String, Object>>>)message.getPayload();
		TreeMap<String, List<HashMap<String, Object>>> sortedMap = new TreeMap<String, List<HashMap<String, Object>>>(unSortedMap);
		
		return sortedMap;
	}
}