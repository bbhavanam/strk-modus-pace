package com.sitetracker.utils;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.sitetracker.beans.PaceXmlPojo;

public class DivideList extends AbstractMessageTransformer {
	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List<PaceXmlPojo> pacexmlPojoList = (List<PaceXmlPojo>) message.getPayload();

		final int size = 1000;// Number of records in each list
		final AtomicInteger counter = new AtomicInteger(0);
		final Collection<List<PaceXmlPojo>> partitioned = pacexmlPojoList.stream().collect(Collectors.groupingBy(it -> counter.getAndIncrement() / size)).values();
		return partitioned;
	}
}