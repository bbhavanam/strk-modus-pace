package com.sitetracker.utils;

import java.util.HashMap;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class MergeMaps extends AbstractMessageTransformer {

	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		List<HashMap<Object, Object>> dataInput = (List<HashMap<Object, Object>>) message.getPayload();
		HashMap<Object, Object> output = new HashMap<Object, Object>();
		for (HashMap<?,?> map : dataInput) {
			output.putAll(map);
		}
		return output;
	}
}