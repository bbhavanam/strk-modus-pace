package com.sitetracker.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;


public class MergeResults extends AbstractMessageTransformer{


@SuppressWarnings({ "unchecked" })
@Override
public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
	

	Iterator<HashMap<Object,Object>> dataInput= ((List<HashMap<Object,Object>>)message.getInvocationProperty("dataInput")).iterator();
	Iterator<HashMap<Object,Object>> results= ((List<HashMap<Object,Object>>)message.getInvocationProperty("results")).iterator();
	List<HashMap<Object,Object>> output= new ArrayList<HashMap<Object,Object>>();
	
	
	while (dataInput.hasNext() || results.hasNext()) {
		HashMap<Object,Object> outMap = new HashMap<Object,Object>();
		outMap.putAll(dataInput.next());
		outMap.putAll(results.next());
		output.add(outMap);
	}
		
	
	message.setInvocationProperty("output" , output);
	
	return output;
	// TODO Auto-generated method stub
}


}