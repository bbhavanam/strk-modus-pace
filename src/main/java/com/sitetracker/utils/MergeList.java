package com.sitetracker.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.sitetracker.beans.PaceXmlPojo;

public class MergeList extends AbstractMessageTransformer {

	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		List<HashMap<Object, Object>> dataInput = (List<HashMap<Object, Object>>) message.getPayload();
		List<HashMap<Object, Object>> output = new ArrayList<HashMap<Object, Object>>();
		for (HashMap<?,?> mapdata : dataInput) {
			List<PaceXmlPojo>beanList=(List<PaceXmlPojo>) mapdata.get("beanList");
			if(beanList.size()==2)
			{
				PaceXmlPojo pojo= new PaceXmlPojo();
				pojo= beanList.get(0);
				for(PaceXmlPojo pacepojofromList:beanList)
				{
					if(pacepojofromList.getTaskAssigneeForecastFinishDate()!=null)
					{
						pojo.setTaskAssigneeForecastFinishDate(pacepojofromList.getTaskAssigneeForecastFinishDate());
					}
					if(pacepojofromList.getActualFinishDate()!=null)
					{
						pojo.setActualFinishDate(pacepojofromList.getActualFinishDate());
					}
				}
				
			}
		}
		return output;
	}
}