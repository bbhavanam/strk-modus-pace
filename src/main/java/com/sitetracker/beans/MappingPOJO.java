package com.sitetracker.beans;

public class MappingPOJO {
	String sObject;
	String salesforceField;
	String relationshipFieldName;
	String direction;
	String isForeCastActualDate;
	String dateFormat;
	
	public String getsObject() {
		return sObject;
	}
	public void setsObject(String sObject) {
		this.sObject = sObject;
	}
	public String getSalesforceField() {
		return salesforceField;
	}
	public void setSalesforceField(String salesforceField) {
		this.salesforceField = salesforceField;
	}
	public String getRelationshipFieldName() {
		return relationshipFieldName;
	}
	public void setRelationshipFieldName(String relationshipFieldName) {
		this.relationshipFieldName = relationshipFieldName;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	public String getIsForeCastActualDate() {
		return isForeCastActualDate;
	}
	public void setIsForeCastActualDate(String isForeCastActualDate) {
		this.isForeCastActualDate = isForeCastActualDate;
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}



	

}