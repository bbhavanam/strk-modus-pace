package com.sitetracker.beans;

import org.apache.commons.lang.StringUtils;

public class PaceXmlPojo {

	public String PaceJobNumber;
	public String TaskCode;
	public String TaskAssigneeForecastFinishDate;
	public String ActualFinishDate;
	public String NeedToBeNA;
	public String getPaceJobNumber() {
		return PaceJobNumber;
	}
	public void setPaceJobNumber(String paceJobNumber) {
		PaceJobNumber = paceJobNumber;
	}
	public String getTaskCode() {
		return TaskCode;
	}
	public void setTaskCode(String taskCode) {
		TaskCode = taskCode;
	}
	public String getTaskAssigneeForecastFinishDate() {
		return TaskAssigneeForecastFinishDate;
	}
	public void setTaskAssigneeForecastFinishDate(String taskAssigneeForecastFinishDate) {
		TaskAssigneeForecastFinishDate = taskAssigneeForecastFinishDate;
	}
	public String getActualFinishDate() {
		return ActualFinishDate;
	}
	public void setActualFinishDate(String actualFinishDate) {
		ActualFinishDate = actualFinishDate;
	}
	public String getNeedToBeNA() {
		return NeedToBeNA;
	}
	public void setNeedToBeNA(String needToBeNA) {
		NeedToBeNA = needToBeNA;
	}

	@Override
	  public boolean equals(Object v) {
	        boolean retVal = false;
	        if (v instanceof PaceXmlPojo){
	        	PaceXmlPojo ptr = (PaceXmlPojo) v;
	            retVal = StringUtils.equalsIgnoreCase(ptr.getPaceJobNumber(), this.PaceJobNumber) && StringUtils.equalsIgnoreCase(ptr.getTaskCode(), this.TaskCode);
	        }
	     return retVal;
	  }
	  
	  @Override
	  public int hashCode() {
		    return java.util.Objects.hashCode(this.PaceJobNumber);
		} 
}

