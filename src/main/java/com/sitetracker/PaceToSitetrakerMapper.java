package com.sitetracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.sitetracker.beans.MappingPOJO;

public class PaceToSitetrakerMapper extends AbstractMessageTransformer {
	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List<MappingPOJO> paceConfigMappingList = (List<MappingPOJO>) message.getPayload();
		List<Map<String, Object>> listRecordsFromPACE = (List<Map<String, Object>>) message.getInvocationProperty("CSVData");
		Map<String, Object> mapRecords = buildMapping(paceConfigMappingList, listRecordsFromPACE);
		//String json_projects = new Gson().toJson(mapRecords);
		message.setInvocationProperty("strkObjects", mapRecords);
		return mapRecords;
	}

	/**
	 * This method returns HashMap with the sobject as key and all the data to
	 * update it as a List of childMaps(HashMap) in value.
	 * 
	 * @param mapConfiguration
	 * @return
	 */
	public Map<String, Object> buildMapping(List<MappingPOJO> paceConfigMappingList, List<Map<String, Object>> listRecordsFromPACE) {
		Map<String, Object> sobjectMap = new HashMap<>();
		;
		List<HashMap<String, Object>> LTEobjectList = new ArrayList<>();
		List<HashMap<String, Object>> NSBobjectList = new ArrayList<>();
		List<HashMap<String, Object>> siteobjectList = new ArrayList<>();
		List<HashMap<String, Object>> strkobjectList = new ArrayList<>();
		for (Map<String, Object> row : listRecordsFromPACE) {
			HashMap<String, Object> fieldMapNSB = new HashMap<>();
			HashMap<String, Object> fieldMapLTE = new HashMap<>();
			HashMap<String, Object> fieldMapSite = new HashMap<>();
			HashMap<String, Object> fieldMapStrk = new HashMap<>();
			for (MappingPOJO mappingpojo : paceConfigMappingList) {
				
				
				if ((StringUtils.equalsIgnoreCase(mappingpojo.getDirection(), "Pull") || StringUtils.equalsIgnoreCase(mappingpojo.getDirection(), "both")) && row.containsKey(mappingpojo.getRelationshipFieldName())) {
					
				 if (StringUtils.equalsIgnoreCase(mappingpojo.getsObject(), "strk__Site__c")){
						if(!"Select".equals(mappingpojo.getIsForeCastActualDate())){
							Date strkDate = convertPaceDateToSTDate((String)row.get(mappingpojo.getRelationshipFieldName()),mappingpojo.getDateFormat());
							if(strkDate != null)
							fieldMapSite.put(mappingpojo.getSalesforceField(), strkDate);
						}else {
							fieldMapSite.put(mappingpojo.getSalesforceField(), row.get(mappingpojo.getRelationshipFieldName()));
						}
					}
					else{
						
						 if(StringUtils.equalsIgnoreCase(mappingpojo.getsObject(), "strk__Project__c")) {
							
							if(!"Select".equals(mappingpojo.getIsForeCastActualDate())){
								Date strkDate = convertPaceDateToSTDate((String)row.get(mappingpojo.getRelationshipFieldName()),mappingpojo.getDateFormat());
								if(strkDate != null && !"".equals(strkDate)){
									fieldMapStrk.put(mappingpojo.getSalesforceField(), strkDate);
								}
							}else{
								fieldMapStrk.put(mappingpojo.getSalesforceField(), row.get(mappingpojo.getRelationshipFieldName()));
							}
							if("strk__Project_Type__c".equals(mappingpojo.getSalesforceField())){
								if("NSB1".equals(row.get(mappingpojo.getRelationshipFieldName()))){
									fieldMapStrk.put(mappingpojo.getSalesforceField(), "NSB");
									
								}else{
									fieldMapStrk.put(mappingpojo.getSalesforceField(), "LTE");
								}
							}
							
							
						}
						
						if(StringUtils.equalsIgnoreCase((String) row.get("JOB_TYPE_SUBTYPE"), "NSB1")){
							
								
								if (StringUtils.equalsIgnoreCase(mappingpojo.getsObject(), "AT_T_NSB_Project__c") ){
									if(!"Select".equals(mappingpojo.getIsForeCastActualDate())){
										Date strkDate = convertPaceDateToSTDate((String)row.get(mappingpojo.getRelationshipFieldName()),mappingpojo.getDateFormat());
										if(strkDate != null)
										fieldMapNSB.put(mappingpojo.getSalesforceField(), strkDate);
									}else{
										fieldMapNSB.put(mappingpojo.getSalesforceField(), row.get(mappingpojo.getRelationshipFieldName()));
									}
									fieldMapNSB.put("PACE_ID_Number__c", fieldMapStrk.get("PACE_ID_Number__c"));
									
									
								}
								
							}else {
								
								if (StringUtils.equalsIgnoreCase(mappingpojo.getsObject(), "AT_T_LTE_Project__c")) {
									
									if(!"Select".equals(mappingpojo.getIsForeCastActualDate())){
										Date strkDate = convertPaceDateToSTDate((String)row.get(mappingpojo.getRelationshipFieldName()),mappingpojo.getDateFormat());
										if(strkDate != null)
											fieldMapLTE.put(mappingpojo.getSalesforceField(), strkDate);
									}else{
										fieldMapLTE.put(mappingpojo.getSalesforceField(), row.get(mappingpojo.getRelationshipFieldName()));
									}
									
									fieldMapLTE.put("PACE_ID_Number__c", fieldMapStrk.get("PACE_ID_Number__c"));
								}
							}
					}
				}
			}
				
			if(!fieldMapLTE.isEmpty())
				LTEobjectList.add(fieldMapLTE);
			
			if(!fieldMapNSB.isEmpty())
				NSBobjectList.add(fieldMapNSB);
			
			if(!fieldMapSite.isEmpty())
			siteobjectList.add(fieldMapSite);
			
			if(!fieldMapStrk.isEmpty())
			strkobjectList.add(fieldMapStrk);
			
		}
		 sobjectMap.put("AT_T_LTE_Project__c", LTEobjectList);
		 sobjectMap.put("AT_T_NSB_Project__c", NSBobjectList);
		 sobjectMap.put("Site__c",siteobjectList);
		 sobjectMap.put("Project__c",strkobjectList);
		return sobjectMap;
	}
	
	public  static Date convertPaceDateToSTDate(String paceDateStr, String formatStr){
		DateFormat df = new SimpleDateFormat("MM/dd/yy");
		Date paceDate;
		Date strkDate = null;
		
		if( !paceDateStr.isEmpty()){
			
			try {
				paceDate = new SimpleDateFormat(formatStr).parse(paceDateStr);
				//paceDate = new SimpleDateFormat("MM/dd/yy").parse(paceDateStr);
				strkDate = df.parse(df.format(paceDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
		return strkDate;
	}

}
