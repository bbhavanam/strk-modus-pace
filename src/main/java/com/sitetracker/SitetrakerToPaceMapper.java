package com.sitetracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.sitetracker.beans.MappingPOJO;

public class SitetrakerToPaceMapper extends AbstractMessageTransformer {
	@SuppressWarnings({ "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List<MappingPOJO> paceConfigMappingList = (List<MappingPOJO>)message.getInvocationProperty("SObjectMappingPOJO");
		List<HashMap<String, Object>> DataToExport = (List<HashMap<String, Object>>)message.getInvocationProperty("DataToExport");
		List<Map<String, Object>> listRecordsFromSTRK = (List<Map<String, Object>>) message.getPayload();
		String strkQueryFields = message.getInvocationProperty("strkQueryFields");
		
		String paceFields = message.getInvocationProperty("paceFields");
		System.out.println("paceFields..." + paceFields);
		List<HashMap<String, Object>> records = buildMapping(paceConfigMappingList, listRecordsFromSTRK, DataToExport, strkQueryFields,  paceFields);
		System.out.println("DataToExport Size..." + records.size());
		message.setInvocationProperty("DataToExport", records);
		return records;
	}

	/**
	 * This method returns HashMap with the sobject as key and all the data to
	 * update it as a List of childMaps(HashMap) in value.
	 * 
	 * @param mapConfiguration
	 * @return
	 */
	public List<HashMap<String, Object>>  buildMapping(List<MappingPOJO> paceConfigMappingList, List<Map<String, Object>> listRecordsFromSTRK, List<HashMap<String, Object>> DataToExport, String strkQueryFields, String paceFields) {
		
	
		List<HashMap<String, Object>> strkobjectList = new ArrayList<>();
		
		for (Map<String, Object> row : listRecordsFromSTRK) {
			
			Map<String, Object> fieldMapStrk = new LinkedHashMap<>();
			Map<String, Object> fieldMapFinal = new LinkedHashMap<>();
			
			for (Map.Entry<String,Object> result : row.entrySet()) {
					if(DataToExport.size() > 0){
						//Add Project Object Data based on configuration mappings
						for(HashMap<String, Object> dataMap : DataToExport) {
							if(dataMap.get(paceFields) != null && dataMap.get(paceFields).equals(result.getValue())){
								//Add PACE_NUMBER as first column and add corresponding project to sub project row based on PACE_NUMBER
								fieldMapStrk.putAll(dataMap);
								fieldMapStrk.putAll(fieldMapFinal);
								fieldMapFinal = fieldMapStrk;
							}
							prepareFieldMap(paceConfigMappingList, result, paceFields, strkQueryFields, fieldMapFinal);
						}
					}
					else{
						//Add Sub Project Object Data based on configuration mappings
						if(strkQueryFields.equals(result.getKey())) {
							//Add PACE_NUMBER as first column and then add other columns
							fieldMapStrk.put(paceFields, result.getValue());
							fieldMapStrk.putAll(fieldMapFinal);
							fieldMapFinal = fieldMapStrk;
							
						}
						prepareFieldMap(paceConfigMappingList, result, paceFields, strkQueryFields, fieldMapFinal);
					}
			}
			if(!fieldMapStrk.isEmpty())
				strkobjectList.add((HashMap<String, Object>) fieldMapFinal);
		}
		
		
		return strkobjectList;
	}
	
	public void  prepareFieldMap(List<MappingPOJO> paceConfigMappingList, Map.Entry<String,Object> result, String paceFields, String strkQueryFields,  Map<String, Object> fieldMapStrk) {
		for (MappingPOJO mappingpojo : paceConfigMappingList) {
			
			if((String)mappingpojo.getSalesforceField() != null  && mappingpojo.getSalesforceField().equals(result.getKey())){
				if(mappingpojo.getRelationshipFieldName() != null)
					fieldMapStrk.put(mappingpojo.getRelationshipFieldName(), result.getValue());
			}
			
			
	}
		
	}

}
